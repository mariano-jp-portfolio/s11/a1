// Function declaration
function greeter(name, email, job, hobby) {
	return "Hi, my name is " + name + ", and my email address is " + email + "." + " I work as a " + job + ", and my hobbies include " + hobby + "."; 
}

console.log(greeter("John", "john@mail.com", "waiter", "singing"));
console.log(greeter("Monika", "monika@mail.com", "writer", "playing piano"));